import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import store from './store/index';

import HighchartsVue from 'highcharts-vue';
import Highcharts from "highcharts";
import Maps from "highcharts/modules/map";
Maps(Highcharts);
import Tilemaps from "highcharts/modules/tilemap";
Tilemaps(Highcharts);

const app = createApp(App);
app.use(router);
app.use(store);
app.use(HighchartsVue);

router.isReady().then(() => {
    app.mount('#app');
});