export default {
  0: {
    "rism_id": 125794,
    "composer": "Paisiello, Giovanni",
    "librettist": "Metastasio, Pietro",
    "title": "Achille in Sciro",
    "performance_year": 1778,
    "placename": "Sankt Petersburg"
  },
  1: {
    "rism_id": 101479,
    "composer": "Mayr, Johann Simon",
    "librettist": "Rossi, Gaetano",
    "title": "Adelaide di Guesclino",
    "performance_year": 1799,
    "placename": "Venezia"
  },
  2: {
    "rism_id": 270001459,
    "composer": "Mayr, Johann Simon",
    "librettist": "Rossi, Gaetano",
    "title": "Adelaide di Guesclino",
    "performance_year": 1808,
    "placename": "Dresden"
  },
  3: {
    "rism_id": 452502381,
    "composer": "Mayr, Johann Simon",
    "librettist": "Rossi, Gaetano",
    "title": "Adelaide di Guesclino",
    "performance_year": 1815,
    "placename": "Hamburg"
  },
  4: {
    "rism_id": 103415,
    "composer": "Anfossi, Pasquale",
    "librettist": "Metastasio, Pietro",
    "title": "Adriano in Siria",
    "performance_year": 1777,
    "placename": "Padova"
  },
  5: {
    "rism_id": 850005576,
    "composer": "Mayr, Johann Simon",
    "librettist": "Metastasio, Pietro",
    "title": "Adriano in Siria",
    "performance_year": 1798,
    "placename": "Venezia"
  },
  6: {
    "rism_id": 400065496,
    "composer": "Paisiello, Giovanni",
    "librettist": "Metastasio, Pietro",
    "title": "Alcide al bivio",
    "performance_year": 1780,
    "placename": "Sankt Petersburg"
  },
  7: {
    "rism_id": 190018321,
    "composer": "Piccinni, Niccolò",
    "librettist": "Metastasio, Pietro",
    "title": "Alessandro nell'Indie",
    "performance_year": 1775,
    "placename": "Napoli"
  },
  8: {
    "rism_id": 300036031,
    "composer": "Cimarosa, Domenico",
    "librettist": "Metastasio, Pietro",
    "title": "Alessandro nell'Indie",
    "performance_year": 1781,
    "placename": "Roma"
  },
  9: {
    "rism_id": 107374,
    "composer": "Paisiello, Giovanni",
    "librettist": "Metastasio, Pietro",
    "title": "Antigono",
    "performance_year": 1785,
    "placename": "Napoli"
  },
  10: {
    "rism_id": 310000019,
    "composer": "Paisiello, Giovanni",
    "librettist": "Metastasio, Pietro",
    "title": "Antigono",
    "performance_year": 1784,
    "placename": "Napoli"
  },
  11: {
    "rism_id": 456009995,
    "composer": "Paisiello, Giovanni",
    "librettist": "Metastasio, Pietro",
    "title": "Antigono",
    "performance_year": 1795,
    "placename": "Napoli"
  },
  12: {
    "rism_id": 840000202,
    "composer": "Anfossi, Pasquale",
    "librettist": "Metastasio, Pietro",
    "title": "Antigono",
    "performance_year": 1782,
    "placename": "Bologna"
  },
  13: {
    "rism_id": 126990,
    "composer": "Cimarosa, Domenico",
    "librettist": "Metastasio, Pietro",
    "title": "Artaserse",
    "performance_year": 1784,
    "placename": "Torino"
  },
  14: {
    "rism_id": 450012678,
    "composer": "Anfossi, Pasquale",
    "librettist": "Metastasio, Pietro",
    "title": "Artaserse",
    "performance_year": 1788,
    "placename": "Roma"
  },
  15: {
    "rism_id": 850010788,
    "composer": "Cimarosa, Domenico",
    "librettist": "Metastasio, Pietro",
    "title": "Artaserse",
    "performance_year": 1785,
    "placename": "Torino"
  },
  16: {
    "rism_id": 852030536,
    "composer": "Piccinni, Niccolò",
    "librettist": "Metastasio, Pietro",
    "title": "Artaserse",
    "performance_year": 1784,
    "placename": "Napoli"
  },
  17: {
    "rism_id": 211010012,
    "composer": "Salieri, Antonio",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Axur re d'Ormus",
    "performance_year": 1789,
    "placename": "Dresden"
  },
  18: {
    "rism_id": 280000106,
    "composer": "Salieri, Antonio",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Axur re d'Ormus",
    "performance_year": 1800,
    "placename": "Weimar"
  },
  19: {
    "rism_id": 400065300,
    "composer": "Salieri, Antonio",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Axur re d'Ormus",
    "performance_year": 1788,
    "placename": "Bonn"
  },
  20: {
    "rism_id": 400065300,
    "composer": "Salieri, Antonio",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Axur re d'Ormus",
    "performance_year": 1803,
    "placename": "Paris"
  },
  21: {
    "rism_id": 451511682,
    "composer": "Salieri, Antonio",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Axur re d'Ormus",
    "performance_year": 1794,
    "placename": "Braunschweig"
  },
  22: {
    "rism_id": 500034287,
    "composer": "Salieri, Antonio",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Axur re d'Ormus",
    "performance_year": 1787,
    "placename": "Paris"
  },
  23: {
    "rism_id": 500034290,
    "composer": "Salieri, Antonio",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Axur re d'Ormus",
    "performance_year": 1786,
    "placename": "Paris"
  },
  24: {
    "rism_id": 500034290,
    "composer": "Salieri, Antonio",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Axur re d'Ormus",
    "performance_year": 1787,
    "placename": "Wien"
  },
  25: {
    "rism_id": 530005383,
    "composer": "Salieri, Antonio",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Axur re d'Ormus",
    "performance_year": 1788,
    "placename": "Wien"
  },
  26: {
    "rism_id": 850010013,
    "composer": "Anfossi, Pasquale",
    "librettist": "Bertati, Giovanni",
    "title": "Azor re di Kibinga",
    "performance_year": 1779,
    "placename": "Venezia"
  },
  27: {
    "rism_id": 455008486,
    "composer": "Cimarosa, Domenico",
    "librettist": "Metastasio, Pietro",
    "title": "Caio Mario",
    "performance_year": 1780,
    "placename": "Roma"
  },
  28: {
    "rism_id": 102137,
    "composer": "Paisiello, Giovanni",
    "librettist": "Metastasio, Pietro",
    "title": "Catone in Utica",
    "performance_year": 1789,
    "placename": "Napoli"
  },
  29: {
    "rism_id": 100380,
    "composer": "Mayr, Johann Simon",
    "librettist": "Rossi, Gaetano",
    "title": "Che originali",
    "performance_year": 1798,
    "placename": "Venezia"
  },
  30: {
    "rism_id": 100380,
    "composer": "Mayr, Johann Simon",
    "librettist": "Rossi, Gaetano",
    "title": "Che originali",
    "performance_year": 1803,
    "placename": "Wien"
  },
  31: {
    "rism_id": 100380,
    "composer": "Mayr, Johann Simon",
    "librettist": "Rossi, Gaetano",
    "title": "Che originali",
    "performance_year": 1815,
    "placename": "Venezia"
  },
  32: {
    "rism_id": 450061889,
    "composer": "Mayr, Johann Simon",
    "librettist": "Rossi, Gaetano",
    "title": "Che originali",
    "performance_year": 1802,
    "placename": "Genova"
  },
  33: {
    "rism_id": 450064907,
    "composer": "Mayr, Johann Simon",
    "librettist": "Rossi, Gaetano",
    "title": "Che originali",
    "performance_year": 1805,
    "placename": "Padova"
  },
  34: {
    "rism_id": 103501,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Così fan tutte",
    "performance_year": 1790,
    "placename": "Wien"
  },
  35: {
    "rism_id": 270001404,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Così fan tutte",
    "performance_year": 1791,
    "placename": "Dresden"
  },
  36: {
    "rism_id": 270001404,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Così fan tutte",
    "performance_year": 1830,
    "placename": "Dresden"
  },
  37: {
    "rism_id": 280000101,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Così fan tutte",
    "performance_year": 1797,
    "placename": "Weimar"
  },
  38: {
    "rism_id": 301000425,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Così fan tutte",
    "performance_year": 1811,
    "placename": "Breslau"
  },
  39: {
    "rism_id": 301000425,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Così fan tutte",
    "performance_year": 1814,
    "placename": "Breslau"
  },
  40: {
    "rism_id": 301000425,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Così fan tutte",
    "performance_year": 1821,
    "placename": "Breslau"
  },
  41: {
    "rism_id": 301000425,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Così fan tutte",
    "performance_year": 1824,
    "placename": "Breslau"
  },
  42: {
    "rism_id": 301000425,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Così fan tutte",
    "performance_year": 1829,
    "placename": "Breslau"
  },
  43: {
    "rism_id": 301000425,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Così fan tutte",
    "performance_year": 1832,
    "placename": "Breslau"
  },
  44: {
    "rism_id": 450018897,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Così fan tutte",
    "performance_year": 1791,
    "placename": "Donaueschingen"
  },
  45: {
    "rism_id": 451501513,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Così fan tutte",
    "performance_year": 1828,
    "placename": "Osnabrück"
  },
  46: {
    "rism_id": 1001035496,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Così fan tutte",
    "performance_year": 1805,
    "placename": "Dessau"
  },
  47: {
    "rism_id": 100459,
    "composer": "Paisiello, Giovanni",
    "librettist": "Metastasio, Pietro",
    "title": "Didone abbandonata",
    "performance_year": 1794,
    "placename": "Napoli"
  },
  48: {
    "rism_id": 850009280,
    "composer": "Anfossi, Pasquale",
    "librettist": "Metastasio, Pietro",
    "title": "Didone abbandonata",
    "performance_year": 1775,
    "placename": "Venezia"
  },
  49: {
    "rism_id": 850009280,
    "composer": "Anfossi, Pasquale",
    "librettist": "Metastasio, Pietro",
    "title": "Didone abbandonata",
    "performance_year": 1788,
    "placename": "Napoli"
  },
  50: {
    "rism_id": 852030379,
    "composer": "Piccinni, Niccolò",
    "librettist": "Metastasio, Pietro",
    "title": "Didone abbandonata",
    "performance_year": 1780,
    "placename": "Napoli"
  },
  51: {
    "rism_id": 103502,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Don Giovanni",
    "performance_year": 1787,
    "placename": "Praha"
  },
  52: {
    "rism_id": 180000585,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Don Giovanni",
    "performance_year": 1794,
    "placename": "Amsterdam"
  },
  53: {
    "rism_id": 270001078,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Don Giovanni",
    "performance_year": 1814,
    "placename": "Dresden"
  },
  54: {
    "rism_id": 270001078,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Don Giovanni",
    "performance_year": 1821,
    "placename": "Dresden"
  },
  55: {
    "rism_id": 270001078,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Don Giovanni",
    "performance_year": 1832,
    "placename": "Dresden"
  },
  56: {
    "rism_id": 280000093,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Don Giovanni",
    "performance_year": 1792,
    "placename": "Weimar"
  },
  57: {
    "rism_id": 452502405,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Don Giovanni",
    "performance_year": 1789,
    "placename": "Hamburg"
  },
  58: {
    "rism_id": 650009939,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Don Giovanni",
    "performance_year": 1788,
    "placename": "Wien"
  },
  59: {
    "rism_id": 101422,
    "composer": "Paisiello, Giovanni",
    "librettist": "Calzabigi, Ranieri de",
    "title": "Elfrida",
    "performance_year": 1792,
    "placename": "Napoli"
  },
  60: {
    "rism_id": 101422,
    "composer": "Paisiello, Giovanni",
    "librettist": "Calzabigi, Ranieri de",
    "title": "Elfrida",
    "performance_year": 1796,
    "placename": "Bologna"
  },
  61: {
    "rism_id": 310000017,
    "composer": "Paisiello, Giovanni",
    "librettist": "Calzabigi, Ranieri de",
    "title": "Elfrida",
    "performance_year": 1782,
    "placename": "Napoli"
  },
  62: {
    "rism_id": 850036126,
    "composer": "Paisiello, Giovanni",
    "librettist": "Calzabigi, Ranieri de",
    "title": "Elfrida",
    "performance_year": 1793,
    "placename": "Napoli"
  },
  63: {
    "rism_id": 851001164,
    "composer": "Paisiello, Giovanni",
    "librettist": "Calzabigi, Ranieri de",
    "title": "Elfrida",
    "performance_year": 1794,
    "placename": "Napoli"
  },
  64: {
    "rism_id": 852029408,
    "composer": "Paisiello, Giovanni",
    "librettist": "Calzabigi, Ranieri de",
    "title": "Elfrida",
    "performance_year": 1796,
    "placename": "Verona"
  },
  65: {
    "rism_id": 450064908,
    "composer": "Mayr, Johann Simon",
    "librettist": "Rossi, Gaetano",
    "title": "Elisa",
    "performance_year": 1804,
    "placename": "Venezia"
  },
  66: {
    "rism_id": 852029222,
    "composer": "Mayr, Johann Simon",
    "librettist": "Rossi, Gaetano",
    "title": "Elisa",
    "performance_year": 1801,
    "placename": "Valletta"
  },
  67: {
    "rism_id": 101462,
    "composer": "Paisiello, Giovanni",
    "librettist": "Calzabigi, Ranieri de",
    "title": "Elvira",
    "performance_year": 1794,
    "placename": "Napoli"
  },
  68: {
    "rism_id": 850032437,
    "composer": "Paisiello, Giovanni",
    "librettist": "Calzabigi, Ranieri de",
    "title": "Elvira",
    "performance_year": 1793,
    "placename": "Napoli"
  },
  69: {
    "rism_id": 452020625,
    "composer": "Meyerbeer, Giacomo",
    "librettist": "Rossi, Gaetano",
    "title": "Emma di Resburgo",
    "performance_year": 1819,
    "placename": "Venezia"
  },
  70: {
    "rism_id": 101487,
    "composer": "Mayr, Johann Simon",
    "librettist": "Rossi, Gaetano",
    "title": "Eraldo ed Emma",
    "performance_year": 1805,
    "placename": "Milano"
  },
  71: {
    "rism_id": 101487,
    "composer": "Mayr, Johann Simon",
    "librettist": "Rossi, Gaetano",
    "title": "Eraldo ed Emma",
    "performance_year": 1809,
    "placename": "Ravenna"
  },
  72: {
    "rism_id": 840000312,
    "composer": "Anfossi, Pasquale",
    "librettist": "Metastasio, Pietro",
    "title": "Ezio",
    "performance_year": 1778,
    "placename": "Venezia"
  },
  73: {
    "rism_id": 280000182,
    "composer": "Mayr, Johann Simon",
    "librettist": "Rossi, Gaetano",
    "title": "Ginevra di Scozia",
    "performance_year": 1811,
    "placename": "Weimar"
  },
  74: {
    "rism_id": 450061890,
    "composer": "Mayr, Johann Simon",
    "librettist": "Rossi, Gaetano",
    "title": "Ginevra di Scozia",
    "performance_year": 1803,
    "placename": "Genova"
  },
  75: {
    "rism_id": 706001250,
    "composer": "Mayr, Johann Simon",
    "librettist": "Rossi, Gaetano",
    "title": "Ginevra di Scozia",
    "performance_year": 1802,
    "placename": "Frankfurt am Main"
  },
  76: {
    "rism_id": 850035287,
    "composer": "Mayr, Johann Simon",
    "librettist": "Rossi, Gaetano",
    "title": "Ginevra di Scozia",
    "performance_year": 1801,
    "placename": "Napoli"
  },
  77: {
    "rism_id": 100159,
    "composer": "Cimarosa, Domenico",
    "librettist": "Sografi, Simeone Antonio",
    "title": "Gli Orazi ed i Curiazi",
    "performance_year": 1796,
    "placename": "Venezia"
  },
  78: {
    "rism_id": 104885,
    "composer": "Cimarosa, Domenico",
    "librettist": "Sografi, Simeone Antonio",
    "title": "Gli Orazi ed i Curiazi",
    "performance_year": 1797,
    "placename": "Venezia"
  },
  79: {
    "rism_id": 210021787,
    "composer": "Cimarosa, Domenico",
    "librettist": "Sografi, Simeone Antonio",
    "title": "Gli Orazi ed i Curiazi",
    "performance_year": 1786,
    "placename": "Leipzig"
  },
  80: {
    "rism_id": 850010622,
    "composer": "Cimarosa, Domenico",
    "librettist": "Sografi, Simeone Antonio",
    "title": "Gli Orazi ed i Curiazi",
    "performance_year": 1797,
    "placename": "Roma"
  },
  81: {
    "rism_id": 850010792,
    "composer": "Cimarosa, Domenico",
    "librettist": "Sografi, Simeone Antonio",
    "title": "Gli Orazi ed i Curiazi",
    "performance_year": 1798,
    "placename": "Milano"
  },
  82: {
    "rism_id": 850035436,
    "composer": "Cimarosa, Domenico",
    "librettist": "Sografi, Simeone Antonio",
    "title": "Gli Orazi ed i Curiazi",
    "performance_year": 1794,
    "placename": "Venezia"
  },
  83: {
    "rism_id": 101507,
    "composer": "Mayr, Johann Simon",
    "librettist": "Rossi, Gaetano",
    "title": "Gli sciti",
    "performance_year": 1800,
    "placename": "Venezia"
  },
  84: {
    "rism_id": 450114584,
    "composer": "Mayr, Johann Simon",
    "librettist": "Rossi, Gaetano",
    "title": "I cherusci",
    "performance_year": 1808,
    "placename": "Roma"
  },
  85: {
    "rism_id": 100458,
    "composer": "Paisiello, Giovanni",
    "librettist": "Bertati, Giovanni",
    "title": "I Filosofi immaginari",
    "performance_year": 1779,
    "placename": "Sankt Petersburg"
  },
  86: {
    "rism_id": 270001228,
    "composer": "Paisiello, Giovanni",
    "librettist": "Bertati, Giovanni",
    "title": "I Filosofi immaginari",
    "performance_year": 1793,
    "placename": "Pillnitz"
  },
  87: {
    "rism_id": 280000042,
    "composer": "Paisiello, Giovanni",
    "librettist": "Bertati, Giovanni",
    "title": "I Filosofi immaginari",
    "performance_year": 1791,
    "placename": "Weimar"
  },
  88: {
    "rism_id": 452023861,
    "composer": "Paisiello, Giovanni",
    "librettist": "Bertati, Giovanni",
    "title": "I Filosofi immaginari",
    "performance_year": 1782,
    "placename": "Venezia"
  },
  89: {
    "rism_id": 840003829,
    "composer": "Paisiello, Giovanni",
    "librettist": "Bertati, Giovanni",
    "title": "I Filosofi immaginari",
    "performance_year": 1779,
    "placename": "Sankt Petersburg"
  },
  90: {
    "rism_id": 840003829,
    "composer": "Paisiello, Giovanni",
    "librettist": "Bertati, Giovanni",
    "title": "I Filosofi immaginari",
    "performance_year": 1780,
    "placename": "Paris"
  },
  91: {
    "rism_id": 101478,
    "composer": "Martín y Soler, Vicente",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Il burbero di buon cuore",
    "performance_year": 1786,
    "placename": "Wien"
  },
  92: {
    "rism_id": 451501547,
    "composer": "Meyerbeer, Giacomo",
    "librettist": "Rossi, Gaetano",
    "title": "Il crociato in Egitto",
    "performance_year": 1833,
    "placename": "Detmold"
  },
  93: {
    "rism_id": 452502388,
    "composer": "Meyerbeer, Giacomo",
    "librettist": "Rossi, Gaetano",
    "title": "Il crociato in Egitto",
    "performance_year": 1824,
    "placename": "Venezia"
  },
  94: {
    "rism_id": 452502388,
    "composer": "Meyerbeer, Giacomo",
    "librettist": "Rossi, Gaetano",
    "title": "Il crociato in Egitto",
    "performance_year": 1832,
    "placename": "Hamburg"
  },
  95: {
    "rism_id": 9482,
    "composer": "Anfossi, Pasquale",
    "librettist": "Bertati, Giovanni",
    "title": "Il curioso indiscreto",
    "performance_year": 1777,
    "placename": "Roma"
  },
  96: {
    "rism_id": 103524,
    "composer": "Paisiello, Giovanni",
    "librettist": "Metastasio, Pietro",
    "title": "Il Demetrio",
    "performance_year": 1779,
    "placename": "Carskoe Selo"
  },
  97: {
    "rism_id": 115125,
    "composer": "Paisiello, Giovanni",
    "librettist": "Metastasio, Pietro",
    "title": "Il Demofoonte",
    "performance_year": 1775,
    "placename": "Venezia"
  },
  98: {
    "rism_id": 452023922,
    "composer": "Paisiello, Giovanni",
    "librettist": "Goldoni, Carlo",
    "title": "Il finto principe",
    "performance_year": 1775,
    "placename": "Firenze"
  },
  99: {
    "rism_id": 102353,
    "composer": "Anfossi, Pasquale",
    "librettist": "Bertati, Giovanni",
    "title": "Il geloso in cimento",
    "performance_year": 1779,
    "placename": "Graz"
  },
  100: {
    "rism_id": 102353,
    "composer": "Anfossi, Pasquale",
    "librettist": "Bertati, Giovanni",
    "title": "Il geloso in cimento",
    "performance_year": 1785,
    "placename": "Castelnuovo"
  },
  101: {
    "rism_id": 102353,
    "composer": "Anfossi, Pasquale",
    "librettist": "Bertati, Giovanni",
    "title": "Il geloso in cimento",
    "performance_year": 1788,
    "placename": "Napoli"
  },
  102: {
    "rism_id": 280000044,
    "composer": "Anfossi, Pasquale",
    "librettist": "Goldoni, Carlo",
    "title": "Il geloso in cimento",
    "performance_year": 1791,
    "placename": "Weimar"
  },
  103: {
    "rism_id": 451501558,
    "composer": "Anfossi, Pasquale",
    "librettist": "Bertati, Giovanni",
    "title": "Il geloso in cimento",
    "performance_year": 1781,
    "placename": "Hamburg"
  },
  104: {
    "rism_id": 451505827,
    "composer": "Anfossi, Pasquale",
    "librettist": "Bertati, Giovanni",
    "title": "Il geloso in cimento",
    "performance_year": 1781,
    "placename": "Braunschweig"
  },
  105: {
    "rism_id": 705000676,
    "composer": "Anfossi, Pasquale",
    "librettist": "Goldoni, Carlo",
    "title": "Il geloso in cimento",
    "performance_year": 1775,
    "placename": "Praha"
  },
  106: {
    "rism_id": 840000350,
    "composer": "Anfossi, Pasquale",
    "librettist": "Bertati, Giovanni",
    "title": "Il geloso in cimento",
    "performance_year": 1779,
    "placename": "Paris"
  },
  107: {
    "rism_id": 840000363,
    "composer": "Anfossi, Pasquale",
    "librettist": "Bertati, Giovanni",
    "title": "Il geloso in cimento",
    "performance_year": 1775,
    "placename": "Dresden"
  },
  108: {
    "rism_id": 840000404,
    "composer": "Anfossi, Pasquale",
    "librettist": "Bertati, Giovanni",
    "title": "Il matrimonio per inganno",
    "performance_year": 1779,
    "placename": "Firenze"
  },
  109: {
    "rism_id": 101224,
    "composer": "Cimarosa, Domenico",
    "librettist": "Bertati, Giovanni",
    "title": "Il matrimonio segreto",
    "performance_year": 1792,
    "placename": "Wien"
  },
  110: {
    "rism_id": 270001328,
    "composer": "Cimarosa, Domenico",
    "librettist": "Bertati, Giovanni",
    "title": "Il matrimonio segreto",
    "performance_year": 1792,
    "placename": "Dresden"
  },
  111: {
    "rism_id": 270001328,
    "composer": "Cimarosa, Domenico",
    "librettist": "Bertati, Giovanni",
    "title": "Il matrimonio segreto",
    "performance_year": 1830,
    "placename": "Dresden"
  },
  112: {
    "rism_id": 280000078,
    "composer": "Cimarosa, Domenico",
    "librettist": "Bertati, Giovanni",
    "title": "Il matrimonio segreto",
    "performance_year": 1796,
    "placename": "Weimar"
  },
  113: {
    "rism_id": 500239739,
    "composer": "Cimarosa, Domenico",
    "librettist": "Bertati, Giovanni",
    "title": "Il matrimonio segreto",
    "performance_year": 1794,
    "placename": "Venezia"
  },
  114: {
    "rism_id": 840003326,
    "composer": "Cimarosa, Domenico",
    "librettist": "Bertati, Giovanni",
    "title": "Il matrimonio segreto",
    "performance_year": 1801,
    "placename": "Paris"
  },
  115: {
    "rism_id": 850036105,
    "composer": "Cimarosa, Domenico",
    "librettist": "Bertati, Giovanni",
    "title": "Il matrimonio segreto",
    "performance_year": 1793,
    "placename": "Napoli"
  },
  116: {
    "rism_id": 1001010498,
    "composer": "Cimarosa, Domenico",
    "librettist": "Bertati, Giovanni",
    "title": "Il matrimonio segreto",
    "performance_year": 1796,
    "placename": "Dessau"
  },
  117: {
    "rism_id": 125792,
    "composer": "Paisiello, Giovanni",
    "librettist": "Goldoni, Carlo",
    "title": "Il mondo della luna",
    "performance_year": 1782,
    "placename": "Sankt Petersburg"
  },
  118: {
    "rism_id": 850008814,
    "composer": "Paisiello, Giovanni",
    "librettist": "Goldoni, Carlo",
    "title": "Il mondo della luna",
    "performance_year": 1783,
    "placename": "Sankt Petersburg"
  },
  119: {
    "rism_id": 450018913,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Metastasio, Pietro",
    "title": "Il re pastore",
    "performance_year": 1775,
    "placename": "Salzburg"
  },
  120: {
    "rism_id": 455024521,
    "composer": "Piccinni, Niccolò",
    "librettist": "Mazzolà, Caterino",
    "title": "Il servo padrone",
    "performance_year": 1794,
    "placename": "Venezia"
  },
  121: {
    "rism_id": 400065517,
    "composer": "Salieri, Antonio",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Il talismano",
    "performance_year": 1788,
    "placename": "Wien"
  },
  122: {
    "rism_id": 452503736,
    "composer": "Salieri, Antonio",
    "librettist": "Goldoni, Carlo",
    "title": "Il talismano",
    "performance_year": 1779,
    "placename": "Milano"
  },
  123: {
    "rism_id": 452503736,
    "composer": "Salieri, Antonio",
    "librettist": "Goldoni, Carlo",
    "title": "Il talismano",
    "performance_year": 1794,
    "placename": "Hamburg"
  },
  124: {
    "rism_id": 451015140,
    "composer": "Martín y Soler, Vicente",
    "librettist": "Metastasio, Pietro",
    "title": "Ipermestra",
    "performance_year": 1780,
    "placename": "Napoli"
  },
  125: {
    "rism_id": 190009493,
    "composer": "Anfossi, Pasquale",
    "librettist": "Bertati, Giovanni",
    "title": "Isabella e Rodrigo",
    "performance_year": 1776,
    "placename": "Venezia"
  },
  126: {
    "rism_id": 450009043,
    "composer": "Anfossi, Pasquale",
    "librettist": "Bertati, Giovanni",
    "title": "Isabella e Rodrigo",
    "performance_year": 1785,
    "placename": "Regensburg"
  },
  127: {
    "rism_id": 101482,
    "composer": "Mayr, Johann Simon",
    "librettist": "Rossi, Gaetano",
    "title": "L' Amor coniugale",
    "performance_year": 1805,
    "placename": "Padova"
  },
  128: {
    "rism_id": 450061891,
    "composer": "Mayr, Johann Simon",
    "librettist": "Rossi, Gaetano",
    "title": "L' Amor coniugale",
    "performance_year": 1807,
    "placename": "Genova"
  },
  129: {
    "rism_id": 102348,
    "composer": "Anfossi, Pasquale",
    "librettist": "Bertati, Giovanni",
    "title": "L' Avaro",
    "performance_year": 1775,
    "placename": "Venezia"
  },
  130: {
    "rism_id": 102348,
    "composer": "Anfossi, Pasquale",
    "librettist": "Bertati, Giovanni",
    "title": "L' Avaro",
    "performance_year": 1777,
    "placename": "Firenze"
  },
  131: {
    "rism_id": 102348,
    "composer": "Anfossi, Pasquale",
    "librettist": "Bertati, Giovanni",
    "title": "L' Avaro",
    "performance_year": 1782,
    "placename": "Braunschweig"
  },
  132: {
    "rism_id": 102348,
    "composer": "Anfossi, Pasquale",
    "librettist": "Bertati, Giovanni",
    "title": "L' Avaro",
    "performance_year": 1787,
    "placename": "Paris"
  },
  133: {
    "rism_id": 450009037,
    "composer": "Anfossi, Pasquale",
    "librettist": "Bertati, Giovanni",
    "title": "L' Avaro",
    "performance_year": 1775,
    "placename": "Regensburg"
  },
  134: {
    "rism_id": 103492,
    "composer": "Martín y Soler, Vicente",
    "librettist": "Da Ponte, Lorenzo",
    "title": "L'arbore di Diana",
    "performance_year": 1787,
    "placename": "Wien"
  },
  135: {
    "rism_id": 280000058,
    "composer": "Martín y Soler, Vicente",
    "librettist": "Da Ponte, Lorenzo",
    "title": "L'arbore di Diana",
    "performance_year": 1793,
    "placename": "Weimar"
  },
  136: {
    "rism_id": 450013125,
    "composer": "Martín y Soler, Vicente",
    "librettist": "Da Ponte, Lorenzo",
    "title": "L'arbore di Diana",
    "performance_year": 1789,
    "placename": "Donaueschingen"
  },
  137: {
    "rism_id": 451507536,
    "composer": "Martín y Soler, Vicente",
    "librettist": "Da Ponte, Lorenzo",
    "title": "L'arbore di Diana",
    "performance_year": 1789,
    "placename": "Braunschweig"
  },
  138: {
    "rism_id": 452502374,
    "composer": "Martín y Soler, Vicente",
    "librettist": "Da Ponte, Lorenzo",
    "title": "L'arbore di Diana",
    "performance_year": 1788,
    "placename": "Hamburg"
  },
  139: {
    "rism_id": 700009206,
    "composer": "Martín y Soler, Vicente",
    "librettist": "Da Ponte, Lorenzo",
    "title": "L'arbore di Diana",
    "performance_year": 1783,
    "placename": "Venezia"
  },
  140: {
    "rism_id": 866914736,
    "composer": "Martín y Soler, Vicente",
    "librettist": "Da Ponte, Lorenzo",
    "title": "L'arbore di Diana",
    "performance_year": 1787,
    "placename": "Wien"
  },
  141: {
    "rism_id": 150203358,
    "composer": "Cimarosa, Domenico",
    "librettist": "Metastasio, Pietro",
    "title": "L'eroe cinese",
    "performance_year": 1782,
    "placename": "Napoli"
  },
  142: {
    "rism_id": 850006954,
    "composer": "Anfossi, Pasquale",
    "librettist": "Bertati, Giovanni",
    "title": "L'imbroglio delle tre spose",
    "performance_year": 1781,
    "placename": "Venezia"
  },
  143: {
    "rism_id": 851000082,
    "composer": "Anfossi, Pasquale",
    "librettist": "Bertati, Giovanni",
    "title": "L'imbroglio delle tre spose",
    "performance_year": 1781,
    "placename": "Firenze"
  },
  144: {
    "rism_id": 101218,
    "composer": "Cimarosa, Domenico",
    "librettist": "Metastasio, Pietro",
    "title": "L'Olimpiade",
    "performance_year": 1784,
    "placename": "Vicenza"
  },
  145: {
    "rism_id": 450022392,
    "composer": "Cimarosa, Domenico",
    "librettist": "Metastasio, Pietro",
    "title": "L'Olimpiade",
    "performance_year": 1784,
    "placename": "Venezia"
  },
  146: {
    "rism_id": 840000372,
    "composer": "Anfossi, Pasquale",
    "librettist": "Goldoni, Carlo",
    "title": "L’incognita perseguitata",
    "performance_year": 1776,
    "placename": "Paris"
  },
  147: {
    "rism_id": 450010309,
    "composer": "Piccinni, Niccolò",
    "librettist": "Goldoni, Carlo",
    "title": "La buona figliuola",
    "performance_year": 1778,
    "placename": "Regensburg"
  },
  148: {
    "rism_id": 450010309,
    "composer": "Piccinni, Niccolò",
    "librettist": "Goldoni, Carlo",
    "title": "La buona figliuola",
    "performance_year": 1782,
    "placename": "Regensburg"
  },
  149: {
    "rism_id": 452503692,
    "composer": "Piccinni, Niccolò",
    "librettist": "Goldoni, Carlo",
    "title": "La buona figliuola",
    "performance_year": 1778,
    "placename": "Hamburg"
  },
  150: {
    "rism_id": 840001565,
    "composer": "Piccinni, Niccolò",
    "librettist": "Goldoni, Carlo",
    "title": "La buona figliuola",
    "performance_year": 1777,
    "placename": "Hannover"
  },
  151: {
    "rism_id": 100184,
    "composer": "Salieri, Antonio",
    "librettist": "Da Ponte, Lorenzo",
    "title": "La cifra",
    "performance_year": 1789,
    "placename": "Wien"
  },
  152: {
    "rism_id": 250000146,
    "composer": "Salieri, Antonio",
    "librettist": "Da Ponte, Lorenzo",
    "title": "La cifra",
    "performance_year": 1793,
    "placename": "Bad Lauchstädt"
  },
  153: {
    "rism_id": 250000146,
    "composer": "Salieri, Antonio",
    "librettist": "Da Ponte, Lorenzo",
    "title": "La cifra",
    "performance_year": 1793,
    "placename": "Weimar"
  },
  154: {
    "rism_id": 270001345,
    "composer": "Salieri, Antonio",
    "librettist": "Da Ponte, Lorenzo",
    "title": "La cifra",
    "performance_year": 1790,
    "placename": "Pillnitz"
  },
  155: {
    "rism_id": 280000091,
    "composer": "Salieri, Antonio",
    "librettist": "Da Ponte, Lorenzo",
    "title": "La cifra",
    "performance_year": 1795,
    "placename": "Weimar"
  },
  156: {
    "rism_id": 452503757,
    "composer": "Salieri, Antonio",
    "librettist": "Da Ponte, Lorenzo",
    "title": "La cifra",
    "performance_year": 1793,
    "placename": "Hamburg"
  },
  157: {
    "rism_id": 230004447,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Mazzolà, Caterino",
    "title": "La clemenza di Tito",
    "performance_year": 1791,
    "placename": "Praha"
  },
  158: {
    "rism_id": 280000103,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Mazzolà, Caterino",
    "title": "La clemenza di Tito",
    "performance_year": 1799,
    "placename": "Weimar"
  },
  159: {
    "rism_id": 451501511,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Mazzolà, Caterino",
    "title": "La clemenza di Tito",
    "performance_year": 1825,
    "placename": "Osnabrück"
  },
  160: {
    "rism_id": 451501511,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Mazzolà, Caterino",
    "title": "La clemenza di Tito",
    "performance_year": 1830,
    "placename": "Osnabrück"
  },
  161: {
    "rism_id": 451506523,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Mazzolà, Caterino",
    "title": "La clemenza di Tito",
    "performance_year": 1799,
    "placename": "Braunschweig"
  },
  162: {
    "rism_id": 453008018,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Mazzolà, Caterino",
    "title": "La clemenza di Tito",
    "performance_year": 1812,
    "placename": "Karlsruhe"
  },
  163: {
    "rism_id": 852030921,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Mazzolà, Caterino",
    "title": "La clemenza di Tito",
    "performance_year": 1809,
    "placename": "Napoli"
  },
  164: {
    "rism_id": 107872,
    "composer": "Anfossi, Pasquale",
    "librettist": "Calzabigi, Ranieri de",
    "title": "La finta giardiniera",
    "performance_year": 1775,
    "placename": "London"
  },
  165: {
    "rism_id": 107872,
    "composer": "Anfossi, Pasquale",
    "librettist": "Calzabigi, Ranieri de",
    "title": "La finta giardiniera",
    "performance_year": 1782,
    "placename": "Frankfurt am Main"
  },
  166: {
    "rism_id": 840000314,
    "composer": "Anfossi, Pasquale",
    "librettist": "Calzabigi, Ranieri de",
    "title": "La finta giardiniera",
    "performance_year": 1778,
    "placename": "Paris"
  },
  167: {
    "rism_id": 840000321,
    "composer": "Anfossi, Pasquale",
    "librettist": "Calzabigi, Ranieri de",
    "title": "La finta giardiniera",
    "performance_year": 1775,
    "placename": "Napoli"
  },
  168: {
    "rism_id": 101433,
    "composer": "Anfossi, Pasquale",
    "librettist": "Bertati, Giovanni",
    "title": "La forza delle donne",
    "performance_year": 1778,
    "placename": "Venezia"
  },
  169: {
    "rism_id": 101433,
    "composer": "Anfossi, Pasquale",
    "librettist": "Bertati, Giovanni",
    "title": "La forza delle donne",
    "performance_year": 1780,
    "placename": "Torino"
  },
  170: {
    "rism_id": 450009040,
    "composer": "Anfossi, Pasquale",
    "librettist": "Bertati, Giovanni",
    "title": "La forza delle donne",
    "performance_year": 1785,
    "placename": "Regensburg"
  },
  171: {
    "rism_id": 451507113,
    "composer": "Anfossi, Pasquale",
    "librettist": "Bertati, Giovanni",
    "title": "La forza delle donne",
    "performance_year": 1781,
    "placename": "Braunschweig"
  },
  172: {
    "rism_id": 120765,
    "composer": "Paisiello, Giovanni",
    "librettist": "Bertati, Giovanni",
    "title": "La locanda",
    "performance_year": 1791,
    "placename": "London"
  },
  173: {
    "rism_id": 132096,
    "composer": "Paisiello, Giovanni",
    "librettist": "Bertati, Giovanni",
    "title": "La locanda",
    "performance_year": 1792,
    "placename": "Napoli"
  },
  174: {
    "rism_id": 851000970,
    "composer": "Mayr, Johann Simon",
    "librettist": "Rossi, Gaetano",
    "title": "La roccia di Frauenstein",
    "performance_year": 1805,
    "placename": "Venezia"
  },
  175: {
    "rism_id": 452503738,
    "composer": "Salieri, Antonio",
    "librettist": "Mazzolà, Caterino",
    "title": "La scuola de' gelosi",
    "performance_year": 1778,
    "placename": "Venezia"
  },
  176: {
    "rism_id": 452503738,
    "composer": "Salieri, Antonio",
    "librettist": "Mazzolà, Caterino",
    "title": "La scuola de' gelosi",
    "performance_year": 1795,
    "placename": "Hamburg"
  },
  177: {
    "rism_id": 103493,
    "composer": "Martín y Soler, Vicente",
    "librettist": "Da Ponte, Lorenzo",
    "title": "La scuola dei maritati",
    "performance_year": 1795,
    "placename": "London"
  },
  178: {
    "rism_id": 103493,
    "composer": "Martín y Soler, Vicente",
    "librettist": "Da Ponte, Lorenzo",
    "title": "La scuola dei maritati",
    "performance_year": 1795,
    "placename": "Venezia"
  },
  179: {
    "rism_id": 103493,
    "composer": "Martín y Soler, Vicente",
    "librettist": "Da Ponte, Lorenzo",
    "title": "La scuola dei maritati",
    "performance_year": 1796,
    "placename": "Wien"
  },
  180: {
    "rism_id": 104444,
    "composer": "Martín y Soler, Vicente",
    "librettist": "Da Ponte, Lorenzo",
    "title": "La scuola dei maritati",
    "performance_year": 1800,
    "placename": "Udine"
  },
  181: {
    "rism_id": 280000120,
    "composer": "Martín y Soler, Vicente",
    "librettist": "Da Ponte, Lorenzo",
    "title": "La scuola dei maritati",
    "performance_year": 1799,
    "placename": "Weimar"
  },
  182: {
    "rism_id": 101217,
    "composer": "Cimarosa, Domenico",
    "librettist": "Goldoni, Carlo",
    "title": "La vanità delusa",
    "performance_year": 1784,
    "placename": "Firenze"
  },
  183: {
    "rism_id": 114379,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Le nozze di Figaro",
    "performance_year": 1786,
    "placename": "Wien"
  },
  184: {
    "rism_id": 250000141,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Le nozze di Figaro",
    "performance_year": 1793,
    "placename": "Weimar"
  },
  185: {
    "rism_id": 270001406,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Le nozze di Figaro",
    "performance_year": 1815,
    "placename": "Dresden"
  },
  186: {
    "rism_id": 270001406,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Le nozze di Figaro",
    "performance_year": 1829,
    "placename": "Dresden"
  },
  187: {
    "rism_id": 451501520,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Le nozze di Figaro",
    "performance_year": 1829,
    "placename": "Detmold"
  },
  188: {
    "rism_id": 463305600,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Le nozze di Figaro",
    "performance_year": 1787,
    "placename": "Praha"
  },
  189: {
    "rism_id": 463305600,
    "composer": "Mozart, Wolfgang Amadeus",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Le nozze di Figaro",
    "performance_year": 1790,
    "placename": "Potsdam"
  },
  190: {
    "rism_id": 101435,
    "composer": "Anfossi, Pasquale",
    "librettist": "Bertati, Giovanni",
    "title": "Lo sposo disperato",
    "performance_year": 1777,
    "placename": "Bologna"
  },
  191: {
    "rism_id": 101435,
    "composer": "Anfossi, Pasquale",
    "librettist": "Bertati, Giovanni",
    "title": "Lo sposo disperato",
    "performance_year": 1777,
    "placename": "Venezia"
  },
  192: {
    "rism_id": 452002422,
    "composer": "Anfossi, Pasquale",
    "librettist": "Bertati, Giovanni",
    "title": "Lo sposo disperato",
    "performance_year": 1778,
    "placename": "Venezia"
  },
  193: {
    "rism_id": 115119,
    "composer": "Anfossi, Pasquale",
    "librettist": "Metastasio, Pietro",
    "title": "Nitteti",
    "performance_year": 1780,
    "placename": "Venezia"
  },
  194: {
    "rism_id": 452023862,
    "composer": "Paisiello, Giovanni",
    "librettist": "Metastasio, Pietro",
    "title": "Nitteti",
    "performance_year": 1777,
    "placename": "Sankt Petersburg"
  },
  195: {
    "rism_id": 452023862,
    "composer": "Paisiello, Giovanni",
    "librettist": "Metastasio, Pietro",
    "title": "Nitteti",
    "performance_year": 1778,
    "placename": "Sankt Petersburg"
  },
  196: {
    "rism_id": 101421,
    "composer": "Paisiello, Giovanni",
    "librettist": "Metastasio, Pietro",
    "title": "Olimpiade",
    "performance_year": 1786,
    "placename": "Napoli"
  },
  197: {
    "rism_id": 450061753,
    "composer": "Cimarosa, Domenico",
    "librettist": "Metastasio, Pietro",
    "title": "Olimpiade",
    "performance_year": 1784,
    "placename": "Vicenza"
  },
  198: {
    "rism_id": 451000073,
    "composer": "Anfossi, Pasquale",
    "librettist": "Metastasio, Pietro",
    "title": "Olimpiade",
    "performance_year": 1778,
    "placename": "Roma"
  },
  199: {
    "rism_id": 456009997,
    "composer": "Paisiello, Giovanni",
    "librettist": "Metastasio, Pietro",
    "title": "Olimpiade",
    "performance_year": 1796,
    "placename": "Napoli"
  },
  200: {
    "rism_id": 101506,
    "composer": "Mayr, Johann Simon",
    "librettist": "Sografi, Simeone Antonio",
    "title": "Saffo",
    "performance_year": 1794,
    "placename": "Venezia"
  },
  201: {
    "rism_id": 280000280,
    "composer": "Rossini, Gioachino",
    "librettist": "Rossi, Gaetano",
    "title": "Semiramide",
    "performance_year": 1825,
    "placename": "Weimar"
  },
  202: {
    "rism_id": 450061016,
    "composer": "Rossini, Gioachino",
    "librettist": "Rossi, Gaetano",
    "title": "Semiramide",
    "performance_year": 1823,
    "placename": "Venezia"
  },
  203: {
    "rism_id": 452503734,
    "composer": "Rossini, Gioachino",
    "librettist": "Rossi, Gaetano",
    "title": "Semiramide",
    "performance_year": 1830,
    "placename": "Hamburg"
  },
  204: {
    "rism_id": 210010000,
    "composer": "Rossini, Gioachino",
    "librettist": "Rossi, Gaetano",
    "title": "Tancredi",
    "performance_year": 1813,
    "placename": "Venezia"
  },
  205: {
    "rism_id": 280000227,
    "composer": "Rossini, Gioachino",
    "librettist": "Rossi, Gaetano",
    "title": "Tancredi",
    "performance_year": 1817,
    "placename": "Weimar"
  },
  206: {
    "rism_id": 101509,
    "composer": "Mayr, Johann Simon",
    "librettist": "Sografi, Simeone Antonio",
    "title": "Telemaco nell'isola di Calipso",
    "performance_year": 1797,
    "placename": "Venezia"
  },
  207: {
    "rism_id": 280000084,
    "composer": "Martín y Soler, Vicente",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Una cosa rara",
    "performance_year": 1791,
    "placename": "Weimar"
  },
  208: {
    "rism_id": 450013124,
    "composer": "Martín y Soler, Vicente",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Una cosa rara",
    "performance_year": 1788,
    "placename": "Donaueschingen"
  },
  209: {
    "rism_id": 451501518,
    "composer": "Martín y Soler, Vicente",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Una cosa rara",
    "performance_year": 1827,
    "placename": "Detmold"
  },
  210: {
    "rism_id": 451501518,
    "composer": "Martín y Soler, Vicente",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Una cosa rara",
    "performance_year": 1829,
    "placename": "Osnabrück"
  },
  211: {
    "rism_id": 451507320,
    "composer": "Martín y Soler, Vicente",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Una cosa rara",
    "performance_year": 1788,
    "placename": "Braunschweig"
  },
  212: {
    "rism_id": 451511607,
    "composer": "Martín y Soler, Vicente",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Una cosa rara",
    "performance_year": 1787,
    "placename": "Wien"
  },
  213: {
    "rism_id": 451511607,
    "composer": "Martín y Soler, Vicente",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Una cosa rara",
    "performance_year": 1795,
    "placename": "Braunschweig"
  },
  214: {
    "rism_id": 456015127,
    "composer": "Martín y Soler, Vicente",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Una cosa rara",
    "performance_year": 1786,
    "placename": "Wien"
  },
  215: {
    "rism_id": 500000034,
    "composer": "Martín y Soler, Vicente",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Una cosa rara",
    "performance_year": 1788,
    "placename": "Venezia"
  },
  216: {
    "rism_id": 850001922,
    "composer": "Martín y Soler, Vicente",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Una cosa rara",
    "performance_year": 1788,
    "placename": "Bologna"
  },
  217: {
    "rism_id": 850008697,
    "composer": "Martín y Soler, Vicente",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Una cosa rara",
    "performance_year": 1789,
    "placename": "Napoli"
  },
  218: {
    "rism_id": 850013578,
    "composer": "Martín y Soler, Vicente",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Una cosa rara",
    "performance_year": 1790,
    "placename": "Faenza"
  },
  219: {
    "rism_id": 852028289,
    "composer": "Martín y Soler, Vicente",
    "librettist": "Da Ponte, Lorenzo",
    "title": "Una cosa rara",
    "performance_year": 1788,
    "placename": "Roma"
  }
}