export default {
    getAll(state) {
        return state.operas;
    },

    getById: (state) => (id) => {
        for (const [key, value] of Object.entries(state.operas)) {
            if (key != id) continue;

            // add the id to the returned object
            return { id: key, ...value };
        }

        return null;
    },

    getByOperaId: (state) => (targetRismId) => {
        const resArray = [];

        for (const [key, value] of Object.entries(state.operas)) {
            if (value.rism_id != targetRismId) continue;

            // add the id to the found object
            resArray.push({ id: key, ...value });
        }

        // return all operas we found
        return resArray;
    },

    getByTitle: (state) => (targetTitle) => {
        const resArray = [];

        for (const [key, value] of Object.entries(state.operas)) {
            if (value.title.toLowerCase() != targetTitle.toLowerCase()) continue;

            // add the id to the found object
            resArray.push({ id: key, ...value });
        }

        // return all operas we found
        return resArray;
    },

    getByComposer: (state) => (targetComposer) => {
        const resArray = [];

        for (const [key, value] of Object.entries(state.operas)) {
            if (value.composer.toLowerCase() != targetComposer.toLowerCase()) continue;

            // add the id to the found object
            resArray.push({ id: key, ...value });
        }

        // return all operas we found
        return resArray;
    },

    getByLibrettist: (state) => (targetLibrettist) => {
        const resArray = [];

        for (const [key, value] of Object.entries(state.operas)) {
            if (value.librettist.toLowerCase() != targetLibrettist.toLowerCase()) continue;

            // add the id to the found object
            resArray.push({ id: key, ...value });
        }

        // return all operas we found
        return resArray;
    },

    getByYear: (state) => (targetYear) => {
        const resArray = [];

        for (const [key, value] of Object.entries(state.operas)) {
            if (value.performance_year != targetYear) continue;

            // add the id to the found object
            resArray.push({ id: key, ...value });
        }

        // return all operas we found
        return resArray;
    },

    getByCity: (state) => (targetCity) => {
        const resArray = [];

        for (const [key, value] of Object.entries(state.operas)) {
            if (value.placename.toLowerCase() != targetCity.toLowerCase()) continue;

            // add the id to the found object
            resArray.push({ id: key, ...value });
        }

        // return all operas we found
        return resArray;
    },

    getByCountry: (state) => (targetCountry) => {
        const resArray = [];
        let targetCities;

        switch (targetCountry.toLowerCase().replace(/[^a-z]/g, '')) {
            case 'austria':
                targetCities = ['Wien', 'Graz', 'Salzburg'];
                break;
            case 'czechrepublic':
                targetCities = ['Praha'];
                break;
            case 'england':
                targetCities = ['London'];
                break;
            case 'france':
                targetCities = ['Paris'];
                break;
            case 'germany':
                targetCities = ['Weimar', 'Dresden', 'Hamburg', 'Braunschweig', 'Regensburg', 'Osnabrück', 'Detmold', 'Donaueschingen', 'Dessau', 'Frankfurt am Main', 'Pillnitz', 'Bad Lauchstädt', 'Bonn', 'Hannover', 'Karlsruhe', 'Leipzig', 'Potsdam'];
                break;
            case 'italy':
                targetCities = ['Venezia', 'Napoli', 'Roma', 'Firenze', 'Bologna', 'Genova', 'Milano', 'Padova', 'Torino', 'Vicenza', 'Castelnuovo', 'Faenza', 'Ravenna', 'Udine', 'Verona'];
                break;
            case 'malta':
                targetCities = ['Valletta'];
                break;
            case 'netherlands':
                targetCities = ['Amsterdam'];
                break;
            case 'poland':
                targetCities = ['Breslau'];
                break;
            case 'russia':
                targetCities = ['Sankt Petersburg', 'Carskoe Selo'];
                break;
            default:
                return [];
        }

        for (const [key, value] of Object.entries(state.operas)) {
            if (!targetCities.includes(value.placename)) continue;

            // add the id to the found object
            resArray.push({ id: key, ...value });
        }

        // return all operas we found
        return resArray;
    },

    getCountry: () => (placename) => {
        placename = placename.toLowerCase().trim();
        if (['wien', 'graz', 'salzburg'].includes(placename)) return 'Austria';
        else if (['praha'].includes(placename)) return 'Czech Republic';
        else if (['london'].includes(placename)) return 'England';
        else if (['paris'].includes(placename)) return 'France';
        else if (['weimar', 'dresden', 'hamburg', 'braunschweig', 'regensburg', 'osnabrück', 'detmold', 'donaueschingen', 'dessau', 'frankfurt am main', 'pillnitz', 'bad lauchstädt', 'bonn', 'hannover', 'karlsruhe', 'leipzig', 'potsdam'].includes(placename)) return 'Germany';
        else if (['venezia', 'napoli', 'roma', 'firenze', 'bologna', 'genova', 'milano', 'padova', 'torino', 'vicenza', 'castelnuovo', 'faenza', 'ravenna', 'udine', 'verona'].includes(placename)) return 'Italy';
        else if (['valletta'].includes(placename)) return 'Malta';
        else if (['amsterdam'].includes(placename)) return 'The Netherlands';
        else if (['breslau'].includes(placename)) return 'Poland';
        else if (['sankt petersburg', 'carskoe selo'].includes(placename)) return 'Russia';
        else return null;
    },
}