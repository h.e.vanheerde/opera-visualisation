import { createStore } from 'vuex';

import mutations from './mutations';
import actions from './actions';
import getters from './getters';
import operaData from './operas';

const store = createStore({
    state() {
        return {
            operas: operaData,
        };
    },

    mutations,
    actions,
    getters
});

export default store;