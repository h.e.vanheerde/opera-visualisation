import { createRouter, createWebHistory } from 'vue-router';

import MainPage from './components/MainPage.vue';
import HelloWorld from './components/HelloWorld.vue';
import GettersTest from './components/GettersTest.vue';
import OperaView from './components/OperaView.vue';
import NotFound from './components/NotFound.vue';

const router = createRouter({
    history: createWebHistory(),

    routes: [
        { path: '/', component: MainPage },
        { path: '/getters', component: GettersTest },
        { path: '/helloworld', component: HelloWorld },
        { path: '/notfound', component: NotFound, props: route => ({ basicLink: route.query.from }) },
        {
            path: '/:id', component: OperaView, props: true, beforeEnter(to) {
                const validIds = [...Array(220).keys()].map(e => e.toString());
                if (validIds.includes(to.params.id)) return true;
                else router.replace(`notfound?from=${to.params.id}`);
            }
        },
        {
            path: '/:catch(.*)', redirect: to => {
                return { path: '/notfound', query: { from: to.path } }
            }
        },
    ],
});

export default router;